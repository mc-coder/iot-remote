package com.point.mina.client;

import java.nio.charset.Charset;

import org.apache.mina.filter.codec.demux.DemuxingProtocolCodecFactory;

import com.point.iot.base.message.PointMessage;
/**
 * Message解码编码工厂
 * @author Langxianwei
 *
 */
public class MessageCodecFactory extends DemuxingProtocolCodecFactory {
	
	public MessageCodecFactory(){
		
	}
	public MessageCodecFactory(Charset charset) {
		addMessageDecoder(new AdminMessageTcpDecoder());
		addMessageEncoder(PointMessage.class, new AdminMessageTcpEncoder());
	}
}
