package com.bootdo.point.domain;

import java.io.Serializable;



/**
 * 设备远程控制
 * 
 * @author langxianwei
 * @email 1992lcg@163.com
 * @date 2018-11-25 18:39:37
 */
public class RemoteDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//设备ID
	private int deviceId;
	//SessionId
	private long sessionId;
	//最大温度
	private short maxTemp;
	//最小温度
	private short minTemp;
	//
	/**
	 * 设置：设备ID
	 */
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * 获取：设备ID
	 */
	public int getDeviceId() {
		return deviceId;
	}
	public long getSessionId() {
		return sessionId;
	}
	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
	public short getMaxTemp() {
		return maxTemp;
	}
	public void setMaxTemp(short maxTemp) {
		this.maxTemp = maxTemp;
	}
	public short getMinTemp() {
		return minTemp;
	}
	public void setMinTemp(short minTemp) {
		this.minTemp = minTemp;
	}
}
