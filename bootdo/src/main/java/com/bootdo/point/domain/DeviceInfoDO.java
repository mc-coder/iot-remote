package com.bootdo.point.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 设备管理
 * 
 * @author langxianwei
 * @email 1992lcg@163.com
 * @date 2018-11-25 18:39:37
 */
public class DeviceInfoDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//设备ID
	private int deviceId;
	//sessionId
	private long sessionId;
	//设备名称
	private String deviceName;
	//在线状态
	private String onlineStatus;
	//下线时间
	private Date offlineTime;
	//设备类型
	private String type;
	//部门/代理商ID
	private int deptId;
	//用户ID
	private int userId;

	public long getSessionId() {
		return sessionId;
	}
	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * 设置：设备ID
	 */
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * 获取：设备ID
	 */
	public int getDeviceId() {
		return deviceId;
	}
	/**
	 * 设置：设备名称
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	/**
	 * 获取：设备名称
	 */
	public String getDeviceName() {
		return deviceName;
	}
	/**
	 * 设置：在线状态
	 */
	public void setOnlineStatus(String onlineStatus) {
		this.onlineStatus = onlineStatus;
	}
	/**
	 * 获取：在线状态
	 */
	public String getOnlineStatus() {
		return onlineStatus;
	}
	/**
	 * 设置：下线时间
	 */
	public void setOfflineTime(Date offlineTime) {
		this.offlineTime = offlineTime;
	}
	/**
	 * 获取：下线时间
	 */
	public Date getOfflineTime() {
		return offlineTime;
	}
	/**
	 * 设置：设备类型
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * 获取：设备类型
	 */
	public String getType() {
		return type;
	}
	public int getDeptId() {
		return deptId;
	}
	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
